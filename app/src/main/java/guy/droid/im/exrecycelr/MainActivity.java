package guy.droid.im.exrecycelr;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {
    RecyclerView recyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        recyclerView = (RecyclerView) findViewById(R.id.recycler_main);
        ArrayList<ParentHeaders> parentHeaders = new ArrayList<>();

        for (int i = 0; i < 10; i++) {
            ArrayList<String> child_headers = new ArrayList<>();
            ParentHeaders parentheadset = new ParentHeaders();

            for (int j = 0; j < 5; j++) {
                child_headers.add(i + " CHILD " + j);
            }
            parentheadset.setChilde_header(child_headers);
            parentheadset.setParent_header("HEAD " + i);
            parentHeaders.add(parentheadset);

        }

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(linearLayoutManager);
        Parent parent = new Parent(MainActivity.this, parentHeaders);
        recyclerView.setAdapter(parent);
    }

    /**
     * PARENT RECYCLER CLASS
     **/

    class Parent extends RecyclerView.Adapter<Parent.ViewHolder> {

        MainActivity activity;
        ArrayList<ParentHeaders> headers;


        public Parent(MainActivity activity, ArrayList<ParentHeaders> headers) {
            this.headers = headers;
            this.activity = activity;

        }


        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

            return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.recycler_parent, parent, false));

        }

        @Override
        public void onBindViewHolder(ViewHolder holder, int position) {


            holder.parent_header.setText(headers.get(position).getParent_header());
            holder.parent_header.setOnClickListener(Detail(position, holder));
            Child child = new Child(activity, headers.get(position).getChilde_header());
            holder.child_recycler.setAdapter(child);
        }

        @Override
        public int getItemCount() {
            return headers.size();
        }

        public View.OnClickListener Detail(final int position, final ViewHolder holder) {
            return new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //  Toast.makeText(activity, ""+position, Toast.LENGTH_SHORT).show();
                    if (holder.expandable_child_layout.getVisibility() == View.VISIBLE) {
                        holder.expandable_child_layout.startAnimation(holder.slide_back);
                        holder.expandable_child_layout.setVisibility(View.GONE);
                    } else {

                        holder.expandable_child_layout.startAnimation(holder.slide);
                        holder.expandable_child_layout.setVisibility(View.VISIBLE);
                    }
                }
            };
        }


        public class ViewHolder extends RecyclerView.ViewHolder {

            TextView parent_header;
            RelativeLayout expandable_child_layout;
            Animation slide, slide_back;
            RecyclerView child_recycler;

            public ViewHolder(View itemView) {
                super(itemView);
                slide = AnimationUtils.loadAnimation(activity, R.anim.slide_top);
                slide_back = AnimationUtils.loadAnimation(activity, R.anim.slide_top_back);
                parent_header = (TextView) itemView.findViewById(R.id.parent_header);
                expandable_child_layout = (RelativeLayout) itemView.findViewById(R.id.expandable_child_layout);
                child_recycler = (RecyclerView) itemView.findViewById(R.id.child_recycler);
                LinearLayoutManager linearLayoutManager = new LinearLayoutManager(activity);
                child_recycler.setLayoutManager(linearLayoutManager);


            }
        }
    }

    /**
     * CHILD RECYCLER MANAGER
     **/

    class Child extends RecyclerView.Adapter<Child.ViewHolder> {
        MainActivity activity;
        ArrayList<String> child_headers;


        public Child(MainActivity activity, ArrayList<String> child_headers) {
            this.child_headers = child_headers;
            this.activity = activity;

        }


        @Override
        public Child.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

            return new Child.ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.recycler_child, parent, false));

        }

        @Override
        public void onBindViewHolder(Child.ViewHolder holder, int position) {

            holder.child_header.setText(child_headers.get(position));
        }

        @Override
        public int getItemCount() {
            return child_headers.size();
        }

        public class ViewHolder extends RecyclerView.ViewHolder {

            TextView child_header;

            public ViewHolder(View itemView) {
                super(itemView);
                child_header = (TextView) itemView.findViewById(R.id.child_head);


            }
        }
    }
}
