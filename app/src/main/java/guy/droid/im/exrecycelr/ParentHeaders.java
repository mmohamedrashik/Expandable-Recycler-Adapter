package guy.droid.im.exrecycelr;

import java.util.ArrayList;

/**
 * Created by admin on 4/1/2017.
 */

public class ParentHeaders {
    String parent_header;
    ArrayList<String> childe_header;

    public ArrayList<String> getChilde_header() {
        return childe_header;
    }

    public void setChilde_header(ArrayList<String> childe_header) {
        this.childe_header = childe_header;
    }


    public String getParent_header() {
        return parent_header;
    }

    public void setParent_header(String parent_header) {
        this.parent_header = parent_header;
    }
}
